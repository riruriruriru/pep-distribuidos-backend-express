const bodyParser = require('body-parser')
const express = require('express')
const router = express.Router()
router.use(bodyParser.json())
var moment = require('moment');
const { Permiso } = require('../model/sequelize')

//app.post('/api/permiso/create', (req, res) => {
//    Permiso.create(req.body)
//        .then(permiso => res.json(permiso))
//})
// get all users
exports.createPermisos= (req, res)=>{
    var fechaLimite = moment().add(3, 'hours');
    console.log(fechaLimite)
    var permiso = {
        nombre: req.body.nombre,
        rut: req.body.rut,
        comuna: req.body.comuna,
        region: req.body.region,
        direccion: req.body.direccion,
        motivos: req.body.motivos,
        fechaPermisoLimite: fechaLimite
    }
    Permiso.create(permiso)
    .then(permiso => {
    res.status(200).send(permiso)})

}
exports.findAllPermisos = (req, res) => {
    Permiso.findAll()
    .then(permisos => {
        res.send(permisos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving divisions."
        });
    });
};
