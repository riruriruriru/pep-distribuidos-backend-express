const Sequelize = require('sequelize')
const PermisoModel = require('./permiso.js')

const sequelize = new Sequelize("postgres://riruriru:riruriru@db:5432/db");

const Permiso = PermisoModel(sequelize, Sequelize)


sequelize.sync({ force: false })
  .then(() => {
    console.log(`Database & tables created!`)
  })

module.exports = {
  Permiso
}
