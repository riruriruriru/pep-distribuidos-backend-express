var express = require('express');
var permisos = express.Router();

var PermisosCtlr = require('../controllers/permiso.js');

permisos.route('/all').get(PermisosCtlr.findAllPermisos)
permisos.route('/add').post(PermisosCtlr.createPermisos)
/* GET users listing. */


module.exports = permisos;
